<?php

require_once 'Conexao.php';

class Usuario
{
    private $id;
    private $nome;
    private $senha;
    private $email;
    private $endereco;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    public function insert(){
        $con = Conexao::conecta();
        $st = $con->prepare("insert into usuario (email, senha, nome, endereco) values (?, ?, ?, ?)");
        $st->bindValue(1, $this->getEmail());
        $st->bindValue(2, $this->getSenha());
        $st->bindValue(3, $this->getNome());
        $st->bindValue(4, $this->getEndereco());
        $con->beginTransaction();
        $st->execute();
        $con->commit();
        $this->setId($con->lastInsertId());
        return $st;
    }
    public function validaEmail(){
        $con = Conexao::conecta();
        $st = $con->prepare("select * from usuario where email = ?");
        $st->bindValue(1, $this->getEmail());
        $st->execute();
        return $st;
    }
    public static function getAll(){
        $con = Conexao::conecta();
        $st = $con->prepare("select id, nome, email, endereco from usuario");
        $st->execute();
        $rows = $st->fetchAll();
        $usuarios = array();
        if($rows){
            foreach($rows as $row){
                $u = new Usuario();
                $u->setId($row['id']);
                $u->setNome($row['nome']);
                $u->setEndereco($row['endereco']);
                $u->setEmail($row['email']);
                $usuarios[] = $u;
            }
        }
        return $usuarios;
    }
    public function getOneByEmail(){
        $con = Conexao::conecta();
        $st = $con->prepare("select * from usuario where email = ?");
        $st->bindValue(1, $this->getEmail());
        $st->execute();
        $row = $st->fetch(PDO::FETCH_ASSOC);

        return $row;
    }
}