<?php


class Conexao
{
    public static function conecta(){
        try{
            $con = new PDO('mysql:host=localhost;dbname=banco_flor_e_cultura', 'root', '');
            return $con;
        }catch (PDOException $e){
            return $e->getMessage();
        }
    }
}