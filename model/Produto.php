<?php
require_once 'Conexao.php';

class Produto
{
    private $id;
    private $nome;
    private $descricao;
    private $imagem;
    private $preco;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    public function insert(){
        $con = Conexao::conecta();
        $st = $con->prepare("insert into produto (nome, descricao, imagem, preco) values (?, ?, ?, ?)");
        $st->bindValue(1, $this->getNome());
        $st->bindValue(2, $this->getDescricao());
        $st->bindValue(3, $this->getImagem());
        $st->bindValue(4, $this->getPreco());
        $con->beginTransaction();
        $st->execute();
        $con->commit();
        $this->setId($con->lastInsertId());
        return $st;
    }
    public static function getAll(){
        $con = Conexao::conecta();
        $st = $con->prepare("select id, nome, descricao, imagem, preco from produto");
        $st->execute();
        $rows = $st->fetchAll();
        $produtos = array();
        if($rows){
            foreach($rows as $row){
                $p = new Produto();
                $p->setId($row['id']);
                $p->setNome($row['nome']);
                $p->setDescricao($row['descricao']);
                $p->setImagem($row['imagem']);
                $p->setPreco($row['preco']);
                $produtos[] = $p;
            }
        }
        return $produtos;
    }

    public static function getOne($id){
        $con = Conexao::conecta();
        $st = $con->prepare("select id, nome, descricao, imagem, preco from produto where id = ?");
        $st->bindValue(1, $id);
        $st->execute();
        $row = $st->fetch(PDO::FETCH_ASSOC);

        $produto = new Produto();

        $produto->setId($row['id']);
        $produto->setNome($row['nome']);
        $produto->setDescricao($row['descricao']);
        $produto->setImagem($row['imagem']);
        $produto->setPreco($row['preco']);

        return $produto;
    }

}