<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Flor & Cultura</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
<?php
require 'nav/navbar.php';
require '../model/Produto.php';
?>

<section class="container mt-3">

    <?php
    if(isset($_SESSION)){
        if(isset($_SESSION['carrinho'])){
            $total = 0;
            $carrinho = $_SESSION['carrinho'];
            $ocorrencias = array_count_values($carrinho);

            echo '<form method="POST" action="../controller/salvarCarrinhoController.php">
            <div class="card">
            <div class="card-header bg-success text-light">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                Meu Carrinho
                <div class="clearfix"></div>
            </div>
            <div class="card-body">';

            foreach ($ocorrencias as $key=>$ocorrencia){
                $produto = new Produto();

                $produto = Produto::getOne($key);

                $total += $produto->getPreco()*$ocorrencia;

                echo '<div class="row">
                <div class="col-xs-2 col-md-2">
                    <img class="card-img-top" src="'.$produto->getImagem().'" alt="'.$produto->getNome().'">
                </div>
                <div class="col-xs-4 col-md-6">
                    <h4 class="product-name"><strong>'.$produto->getNome().'</strong></h4><h4><small>'.$produto->getDescricao().'</small></h4>
                </div>
                <div class="col-xs-6 col-md-4 row">
                    <div class="col-xs-6 col-md-6 text-right" style="padding-top: 5px">
                        <h6>R$ <strong>'.$produto->getPreco().'<span class="text-muted">x</span></strong></h6>
                    </div>
                    <div class="col-xs-4 col-md-4">
                        <input type="text" class="form-control input-sm" name="'.$key.'" value="'.$ocorrencia.'">
                    </div>
                </div>
            </div>
            <hr>';
            }
            echo '</div>
            <div class="card-footer">
                <a href="telaFinalizarCompraView.php" class="btn btn-success pull-right">Finalizar Compra</a>
            <input type="submit" class="btn btn-light border-dark pull-right mr-2" value="Salvar meu carrinho">
            <div class="pull-right mr-5">
                Total: R$ <strong>'.$total.'</strong>
            </div>
            </div>
        </div>
        </form>';

        }else{
            echo "<div class=\"alert alert-danger\" role=\"alert\">Carrinho vazio!</div>";
        }
    }else{
        echo "<div class=\"alert alert-danger\" role=\"alert\">Carrinho vazio!</div>";
    }

    ?>


</section>

<!-- JS Bootstrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/c560c025cf.js"></script>

</body>
</html>