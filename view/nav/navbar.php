<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <a class="navbar-brand text-white">Flor & Cultura</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="telaPrincipalView.php">Produtos <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <?php
            session_start();
            if(isset($_SESSION['login'])){
                echo "
                    <a class=\"btn btn-success\" href=\"telaMeuCarrinhoView.php\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a>
                    <div class='text-white mr-1 ml-1'>$_SESSION[login]</div>
                    <form class=\"form-inline \" action=\"../controller/LogoutController.php\">
                        <button class=\"btn btn-success my-2 my-sm-0\" type=\"submit\">Logout</button>
                    </form>";
            }else{
                echo "
                    <form class=\"form-inline \" action=\"telaLoginView.php\">
                        <button class=\"btn btn-success my-2 my-sm-0\" type=\"submit\">Login</button>
                    </form>
                    <form class=\"form-inline \" action=\"telaCadastroView.php\">
                        <button class=\"btn btn-success my-2 my-sm-0\" type=\"submit\">Cadastrar-se</button>
                    </form>";
            }
        ?>

    </div>
    <script src="https://use.fontawesome.com/c560c025cf.js"></script>
</nav>