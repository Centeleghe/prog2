<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Flor & Cultura</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<?php
require 'nav/navbar.php';
require '../model/Produto.php';
?>

<section class="container mt-3">
    <h1>Flor & Cultura produtos</h1>
    <hr>

    <div class="row">
        <?php
        $getter = new Produto();
        $produtos = $getter->getAll();

        foreach ($produtos as $produto) {
            echo '<div class="col-md-4">
                <div class="card mb-4 border-success">
                    <a href="telaProdutoView.php?id='.$produto->getId().'"><img class="card-img-top"  src= "'. $produto->getImagem() .'" alt="'.$produto->getNome().'"></a>
                    <div class="card-body">
                        <h5 class="card-title">'.$produto->getNome().'</h5>
                        <div class="font-weight-bold mb-2">R$ '.$produto->getPreco().'</div>
                        <p class="card-text">'.$produto->getDescricao().'</p>
                        <a href="../controller/AdicionarCarrinhoController.php?id='.$produto->getId().'" class="btn btn-success btn-sm">Adicionar no carrinho</a>
                    </div>
                </div>
            </div>';
        }
        ?>
    </div>

</section>

<!-- JS Bootstrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>