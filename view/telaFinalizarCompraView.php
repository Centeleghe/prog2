<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Flor & Cultura</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<?php
require 'nav/navbar.php';
require '../model/Produto.php';
?>


<body>

<section class="container mt-3">

    <?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    require 'vendor/autoload.php';
    require_once '../model/Usuario.php';

    echo "<div class=\"alert alert-success\" role=\"alert\">Compra Realizada com sucesso!</div>";

    if(isset($_SESSION)){
        if(isset($_SESSION['carrinho'])){
            $total = 0;
            $carrinho = $_SESSION['carrinho'];
            $ocorrencias = array_count_values($carrinho);

            echo '
            <div class="card">
            <div class="card-header bg-success text-light">
                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                Minha compra
                <div class="clearfix"></div>
            </div>
            <div class="card-body">';

            foreach ($ocorrencias as $key=>$ocorrencia){
                $produto = new Produto();

                $produto = Produto::getOne($key);

                $total += $produto->getPreco()*$ocorrencia;

                echo '<div class="row">
                <div class="col-xs-2 col-md-2">
                    <img class="card-img-top" src="'.$produto->getImagem().'" alt="'.$produto->getNome().'">
                </div>
                <div class="col-xs-4 col-md-6">
                    <h4 class="product-name"><strong>'.$produto->getNome().'</strong></h4><h4><small>'.$produto->getDescricao().'</small></h4>
                </div>
                <div class="col-xs-6 col-md-4 row">
                    <div class="col-xs-6 col-md-6 text-right" style="padding-top: 5px">
                        <h6>R$ <strong>'.$produto->getPreco().'<span class="text-muted">x</span></strong></h6>
                    </div>
                    <div class="col-xs-4 col-md-4">'.$ocorrencia.'</div>
                </div>
            </div>
            <hr>';
            }
            echo '</div>
            <div class="card-footer">
                <a href="telaPrincipalView.php" class="btn btn-success pull-right">Menu Principal</a>
            <div class="pull-right mr-5">
                Total: R$ <strong>'.$total.'</strong>
            </div>
            </div>
        </div>';

        }else{
            header('location: telaPrincipalView.php');
        }
    }else{
        echo "<div class=\"alert alert-danger\" role=\"alert\">Carrinho vazio!</div>";
    }

    //mandando email

    //testezao
    // Configuring SMTP server settings
    $mail = new PHPMailer(true);
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = 'topodecadeia@gmail.com';
    $mail->Password = 'blackout17';
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $to_id = $_SESSION['email'];
    $subject = 'Compra Flor&Cultura';
    $message = "Olá " . $_SESSION['login'] . "!" . "<br><br>Obrigado por efetuar a compra conosco de:<br>";
    foreach ($ocorrencias as $key=>$ocorrencia) {
        $produto = new Produto();

        $produto = Produto::getOne($key);

        $message = $message . $produto->getNome() . "  (x " . $ocorrencia . ") <br>";
    }
    $message = $message . "<br> Totalizando: R$ ". $total . ".";
    // Email Sending Details
    $mail->setFrom('topodecadeia@gmail.com', $subject);
    $mail->addAddress($to_id);
    $mail->Subject = $subject;
    $mail->msgHTML($message);

    // Success or Failure
    if (!$mail->send()) {
        $error = "Mailer Error: " . $mail->ErrorInfo;
        echo '<p id="para">'.$error.'</p>';
    }
    else {

    }


    unset($_SESSION['carrinho']);

    ?>

</section>

<!-- JS Bootstrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/c560c025cf.js"></script>

</body>
</html>
