<?php
session_start();
{
    if (isset($_SESSION['login'])) {
        session_destroy();
        session_unset();
        session_abort();
        header("Location: ../view/TelaPrincipalView.php");
    } else {
        header("Location: ../view/TelaPrincipalView.php");
    }
}