<?php
    require_once '../model/Usuario.php';
    session_start();
    $email = @$_POST['email'];
    $senha = @$_POST['senha'];
    if($_POST['email'] != NULL and $_POST['senha'] != NULL){
        $usuario = new Usuario();
        $usuario->setEmail($email);
        $linha = $usuario->getOneByEmail();

        if($linha == NULL or !password_verify($senha, $linha['senha'])){
            header('Location: ../view/telaLoginView.php?mensagem=Dados Inválidos!');
        }else{
            $_SESSION['login'] = $linha['nome'];
            $_SESSION['email'] = $linha['email'];
            header('Location: ../view/telaPrincipalView.php');
        }
    }else{
        header('Location: ../view/telaLoginView.php?mensagem=Preencha todos os campos!');
    }