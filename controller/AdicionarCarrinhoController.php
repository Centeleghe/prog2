<?php
session_start();
if(!isset($_SESSION['login'])){
    header('Location: ../view/telaLoginView.php?mensagem=Faça login para utilizar o carrinho!');
}else{
    if(isset($_GET['id'])){
        $id=$_GET['id'];
    }
    else{
        header("Location: ../view/telaPrincipalView.php");
    }
    if(!isset($_SESSION['carrinho'])){
        $_SESSION['carrinho'] = array();
        $_SESSION['carrinho'][]=$id;
        header('Location: ../view/telaMeuCarrinhoView.php');
    }
    else{
        $_SESSION['carrinho'][]=$id;
        header('Location: ../view/telaMeuCarrinhoView.php');
    }
}
?>
