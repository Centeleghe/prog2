<?php
    require_once '../model/Usuario.php';

    session_start();
    $email = @$_POST['email'];

    if($_POST['nome'] != NULL and $_POST['senha'] != NULL and $_POST['email'] != NULL and $_POST['endereco'] != NULL){
        $usuario = new Usuario();
        $usuario->setEmail($_POST['email']);
        $rows = $usuario->validaEmail();
        $row = $rows->fetch(PDO::FETCH_ASSOC);
    //    print_r($row);
        if(!isset($row['email'])){
            $usuario->setNome($_POST['nome']);
            $usuario->setEndereco($_POST['endereco']);
            $usuario->setSenha(password_hash($_POST['senha'], PASSWORD_DEFAULT));
            $usuario->insert();
            header('Location: ../view/telaPrincipalView.php');
        }else{
            header('Location: ../view/telaCadastroView.php?mensagem=Email já cadastrado!');
        }
    }else{
        header('Location: ../view/telaCadastroView.php?mensagem=Preencha todos os campos!');
    }